class Deck < ApplicationRecord
  has_many :images, as: :imageable, :class_name => "Deck::Asset", dependent: :destroy
  accepts_nested_attributes_for :images, allow_destroy: true
end
