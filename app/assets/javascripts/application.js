// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require plugins/jquery.readySelector
//= require ckeditor/init
//= require bootstrap
//= require light-gallery
//= require owl.carousel.min
//= require imagesloaded.min
//= require masonry.min
//= require turbolinks
//= require_tree .

$(document).on('turbolinks:load', function() {
  if ($('body.home.index').length > 0) {
    console.log('index');
    // $('.ypassport').addClass('badges');
    // $('.review-content').addClass('badges');
  }

  if ($('body.home.about').length > 0) {
    console.log('about');
  }

  $('.gallery-item').hover(
    function(){
      $(this).find(".cover-item-gallery").fadeIn();
    },
    function(){
      $(this).find(".cover-item-gallery").fadeOut();
    }
  );

  // masonryjs
  var $grid = $('.grid').imagesLoaded( function() {
    $grid.masonry({
      itemSelector: '.grid-item',
      percentPosition: true,
      columnWidth: '.grid-sizer'
    }); 
  });

    // Select all links with hashes
    $('a[href*="#"]')
    // Remove links that don't actually link to anything or links that you dont want to scroll (bootstrap)
    .not('[href="#"]')
    .not('[href="#0"]')
    .not('[href="#carousel-example-generic"]')
    .not('[href="#carousel-example-generic1"]')
    .not('[href="#myCarousel"]')
    .click(function(event) {
      // On-page links
      if (
        location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') 
        && 
        location.hostname == this.hostname
      ) {
        // Figure out element to scroll to
        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
        // Does a scroll target exist?
        if (target.length) {
          // Only prevent default if animation is actually gonna happen
          event.preventDefault();
          $('html, body').animate({
            scrollTop: target.offset().top
          }, 2000, function() {
            // // THIS SECTION ADDS A FOCUS TO THE AREA SCROLLED - ALEX
            // // Callback after animation
            // // Must change focus!
            // var $target = $(target);
            // $target.focus();
            // if ($target.is(":focus")) { // Checking if the target was focused
            //   return false;
            // } else {
            //   $target.attr('tabindex','-1'); // Adding tabindex for elements not focusable
            //   $target.focus(); // Set focus again
            // };
          });
        }
      }
    });

});
