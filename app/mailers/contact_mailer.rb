class ContactMailer < ApplicationMailer
  default from: ENV["GMAIL_USERNAME"]

  def welcome_email(contact)
    @contact = contact
    mail(to: "ladecks@gmail.com", subject: 'You have a new contact for LA-Decks')
  end
end
