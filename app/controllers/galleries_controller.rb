class GalleriesController < ApplicationController
  before_action :authenticate_user!, only: [:new, :create, :update, :destroy]
   
  def decks
    @deck = Deck.where(name: "decks").first
    @images = Image.where(imageable_type: "Deck", imageable_id: @deck.id)
  end

  def pools
    @deck = Deck.where(name: "pools").first
    @images = Image.where(imageable_type: "Deck", imageable_id: @deck.id)
  end

  def hillside_decks
    @deck = Deck.where(name: "hillside-decks").first
    @images = Image.where(imageable_type: "Deck", imageable_id: @deck.id)
  end

  def railing
    @deck = Deck.where(name: "railing").first
    @images = Image.where(imageable_type: "Deck", imageable_id: @deck.id)
  end

  def hardscape
    @deck = Deck.where(name: "hardscape").first
    @images = Image.where(imageable_type: "Deck", imageable_id: @deck.id)
  end

  def fences
    @deck = Deck.where(name: "fences").first
    @images = Image.where(imageable_type: "Deck", imageable_id: @deck.id)
  end

  def patio_covers
    @deck = Deck.where(name: "patio-covers").first
    @images = Image.where(imageable_type: "Deck", imageable_id: @deck.id)
  end

  def commercial_decks
    @deck = Deck.where(name: "commercial-decks").first
    @images = Image.where(imageable_type: "Deck", imageable_id: @deck.id)
  end

  def other_projects
    @deck = Deck.where(name: "other-projects").first
    @images = Image.where(imageable_type: "Deck", imageable_id: @deck.id)
  end



  def ipe_decks
    @deck = Deck.where(name: "ipe-decks").first
    @images = Image.where(imageable_type: "Deck", imageable_id: @deck.id)
  end

  def azek_decks
    @deck = Deck.where(name: "azek-decks").first
    @images = Image.where(imageable_type: "Deck", imageable_id: @deck.id)
  end

  def trex_decks
    @deck = Deck.where(name: "trex-decks").first
    @images = Image.where(imageable_type: "Deck", imageable_id: @deck.id)
  end

  def mangaris_decks
    @deck = Deck.where(name: "mangaris-decks").first
    @images = Image.where(imageable_type: "Deck", imageable_id: @deck.id)
  end



  def pools_new
    @deck = Deck.where(name: "pools-new").first
    @images = Image.where(imageable_type: "Deck", imageable_id: @deck.id)
  end

  def pools_remodel
    @deck = Deck.where(name: "pools-remodel").first
    @images = Image.where(imageable_type: "Deck", imageable_id: @deck.id)
  end


  def railing_glass
    @deck = Deck.where(name: "railing-glass").first
    @images = Image.where(imageable_type: "Deck", imageable_id: @deck.id)
  end

  def railing_cable
    @deck = Deck.where(name: "railing-cable").first
    @images = Image.where(imageable_type: "Deck", imageable_id: @deck.id)
  end

  def railing_other
    @deck = Deck.where(name: "railing-other").first
    @images = Image.where(imageable_type: "Deck", imageable_id: @deck.id)
  end

  # def deck_railing
  #   @deck = Deck.where(name: "deck-railing").first
  #   @images = Image.where(imageable_type: "Deck", imageable_id: @deck.id)
  # end

  # def fences_and_railing
  #   @deck = Deck.where(name: "fences-and-railing").first
  #   @images = Image.where(imageable_type: "Deck", imageable_id: @deck.id)
  # end
  
  # def patio
  #   @deck = Deck.where(name: "patio").first
  #   @images = Image.where(imageable_type: "Deck", imageable_id: @deck.id)
  # end




  def new
    @deck = Deck.where(name: params[:deckname]).first
  end

  def create
    @deck = Deck.where(name: params[:deckname]).first
    params[:pics].each do |picture|
      @deck.images.create(pic: picture)
    end if params[:pics].present? 
    redirect_to root_path + "/#{params[:deckname]}"
  end

  def update
    @deck = Deck.where(name: params[:deckname]).first
    params[:pics].each do |picture|
      @deck.images.create(pic: picture)
    end if params[:pics].present? 
    redirect_to "/#{@deck.name}"
  end

  def destroy
    deck = Deck.find(params[:mydeck].to_i)
    image = deck.images.find(params[:myimage].to_i)
    image.destroy
    redirect_to :back, notice: 'Image was successfully destroyed.'
  end

  private


  def deck_params
    params.require(:deck).permit(:name, images_attributes: [:id, :pic, :name, :_destroy])
  end


end