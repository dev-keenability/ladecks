class HomeController < ApplicationController

  def index
    @blogs = Blog.last(3)
  end

  def about
  end

end
