Rails.application.routes.draw do
  require "admin_constraint"
  mount Ckeditor::Engine => "/ckeditor", constraints: AdminConstraint.new

  #########################################
  #==news url for blog model
  #########################################
    get "/blogs", to: "blogs#index", as: :blogs
    post "blogs", to: "blogs#create"
    get "/blogs/new", to: "blogs#new", as: :new_blog
    get "blogs/:title_for_slug/edit", to: "blogs#edit", as: "edit_blog"
    get "blogs/:title_for_slug", to: "blogs#show", as: "blog"
    patch "blogs/:title_for_slug", to: "blogs#update"
    put "blogs/:title_for_slug", to: "blogs#update"
    delete "blogs/:title_for_slug", to: "blogs#destroy"
  
  devise_scope :user do
    get "/users/sign_up",  :to => "devise/sessions#new"
  end
  
  devise_for :users

  devise_scope :user do 
    get "/admin" => "devise/sessions#new" 
  end
  
  root 'home#index'

  match '/about-us'              =>        'home#about',              via: [:get],            :as => 'about'
  match '/gallery'              =>        'home#gallery',              via: [:get],            :as => 'gallery'
  match '/services'              =>        'home#services',              via: [:get],            :as => 'services'
  match '/contact'              =>        'home#contact',              via: [:get],            :as => 'contact'

  match '/decks'              =>        'galleries#decks',              via: [:get],            :as => 'decks'
  match '/pools'              =>        'galleries#pools',              via: [:get],            :as => 'pools'
  match '/hillside-decks'              =>        'galleries#hillside_decks',              via: [:get],            :as => 'hillside_decks'
  match '/railing'              =>        'galleries#railing',              via: [:get],            :as => 'railing'
  match '/hardscape'              =>        'galleries#hardscape',              via: [:get],            :as => 'hardscapes'
  match '/fences'              =>        'galleries#fences',              via: [:get],            :as => 'fences'
  match '/patio-covers'              =>        'galleries#patio_covers',              via: [:get],            :as => 'patio_covers'
  match '/commercial-decks'              =>        'galleries#commercial_decks',              via: [:get],            :as => 'commercial_decks'
  match '/other-projects'              =>        'galleries#other_projects',              via: [:get],            :as => 'other_projects'
  
  match '/ipe-decks'              =>        'galleries#ipe_decks',              via: [:get],            :as => 'ipe_decks'
  match '/azek-decks'              =>        'galleries#azek_decks',              via: [:get],            :as => 'azek_decks'
  match '/trex-decks'              =>        'galleries#trex_decks',              via: [:get],            :as => 'trex_decks'
  match '/mangaris-decks'              =>        'galleries#mangaris_decks',              via: [:get],            :as => 'mangaris_decks'

  match '/pools-new'              =>        'galleries#pools_new',              via: [:get],            :as => 'pools_new'
  match '/pools-remodel'              =>        'galleries#pools_remodel',              via: [:get],            :as => 'pools_remodel'

  match '/railing-glass'              =>        'galleries#railing_glass',              via: [:get],            :as => 'railing_glass'
  match '/railing-cable'              =>        'galleries#railing_cable',              via: [:get],            :as => 'railing_cable'
  match '/railing-other'              =>        'galleries#railing_other',              via: [:get],            :as => 'railing_other'

  # match '/deck-railing'              =>        'galleries#deck_railing',              via: [:get],            :as => 'deck_railing'
  # match '/fences-and-railing'              =>        'galleries#fences_and_railing',              via: [:get],            :as => 'fences_and_railing'
  # match '/patio'              =>        'galleries#patio',              via: [:get],            :as => 'patio'


  match "/contacts"              =>        "contacts#create",              via: [:post]
  
  match "/new-image"              =>        "galleries#new",              via: [:get],    :as => 'new_image'
  match "/create-image"              =>        "galleries#create",              via: [:post]
  match "/update-image"              =>        "galleries#update",              via: [:patch]
  match "/delete-image"             =>        "galleries#destroy",              via: [:delete], :as => 'delete_image'
end
