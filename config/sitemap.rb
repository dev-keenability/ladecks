# Set the host name for URL creation
SitemapGenerator::Sitemap.default_host = "http://www.la-decks.com"


SitemapGenerator::Sitemap.create do
  # Put links creation logic here.
  #
  # The root path '/' and sitemap index file are added automatically for you.
  # Links are added to the Sitemap in the order they are specified.
  #
  # Usage: add(path, options={})
  #        (default options are used if you don't specify)
  #
  # Defaults: :priority => 0.5, :changefreq => 'weekly',
  #           :lastmod => Time.now, :host => default_host
  #
  # Examples:
  #
  # Add '/articles'
  #
  #   add articles_path, :priority => 0.7, :changefreq => 'daily'
  #
  # Add all articles:
  #
  #   Article.find_each do |article|
  #     add article_path(article), :lastmod => article.updated_at
  #   end


  add blogs_path, :priority => 0.8, :changefreq => 'weekly'
  Blog.find_each do |blog|
    add blog_path(blog.title_for_slug), :changefreq => 'weekly', :lastmod => blog.updated_at, :priority => 0.8
  end

  add about_path, :priority => 0.8, :changefreq => 'monthly'
  add gallery_path, :priority => 0.8, :changefreq => 'monthly'
  add services_path, :priority => 0.8, :changefreq => 'monthly'
  add contact_path, :priority => 0.8, :changefreq => 'monthly'
  add decks_path, :priority => 0.8, :changefreq => 'monthly'
  add pools_path, :priority => 0.8, :changefreq => 'monthly'
  add hillside_decks_path, :priority => 0.8, :changefreq => 'monthly'
  add railing_path, :priority => 0.8, :changefreq => 'monthly'
  add hardscapes_path, :priority => 0.8, :changefreq => 'monthly'
  add fences_path, :priority => 0.8, :changefreq => 'monthly'
  add patio_covers_path, :priority => 0.8, :changefreq => 'monthly'
  add commercial_decks_path, :priority => 0.8, :changefreq => 'monthly'
  add other_projects_path, :priority => 0.8, :changefreq => 'monthly'
  add ipe_decks_path, :priority => 0.8, :changefreq => 'monthly'
  add azek_decks_path, :priority => 0.8, :changefreq => 'monthly'
  add trex_decks_path, :priority => 0.8, :changefreq => 'monthly'
  add mangaris_decks_path, :priority => 0.8, :changefreq => 'monthly'
  add pools_new_path, :priority => 0.8, :changefreq => 'monthly'
  add pools_remodel_path, :priority => 0.8, :changefreq => 'monthly'
  add railing_glass_path, :priority => 0.8, :changefreq => 'monthly'
  add railing_cable_path, :priority => 0.8, :changefreq => 'monthly'
  add railing_other_path, :priority => 0.8, :changefreq => 'monthly'



  #Note make sure you run rake sitemap:create in production (if heroku: heroku run rake sitemap:create) as the blogs and listings could be different in production
  #This will create public/sitemap.xml.gz
  #However heroku's ephemeral filesystem wont allow you to create files on their system, therefore your best bet would
  #be to store it on amazon (note as a temp fix, you can clone your production database into your development databse and run rake sitemap:create and then push it to heroku, but then again you wont get the updates).
  #To learn more about this: https://www.cookieshq.co.uk/posts/creating-a-sitemap-with-ruby-on-rails-and-upload-it-to-amazon-s3 

  #sitemap:create   #Create sitemaps without pinging search engines
  #sitemap:refresh  #Create sitemaps and ping search engines
  #sitemap:clean    #Clean up sitemaps in the sitemap path

  # Here is an example of how to extract the contents of a gzip file:

    # gzip -d file.gz

  # Here is an example of how to extract the contents of a tar file:

    # tar xvf file.tar

  # Here are examples of how to extract the contents of a tar.gz and a tgz file:

    # gzip -d < file.tar.gz | tar xvf - 
    # gzip -d < file.tgz | tar xvf -

  #To view your sitemap from the cli

    # curl -sH 'Accept-encoding: gzip' http://www.la-decks.com/sitemap.xml.gz | gunzip - | xmllint --format -
    # the xmllint portion is to pretty print xml from the cli


end

