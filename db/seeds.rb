# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)





decks = Deck.create(name: 'decks')
i = 1
Dir.foreach("#{Rails.root}/app/assets/images/decks-decks") do |item|
  next if item == '.' or item == '..'
  decks.images.create({pic: File.new("#{Rails.root}/app/assets/images/decks-decks/#{item}"), position: i})
  i += 1
end

hillside_decks = Deck.create(name: 'hillside-decks')

deck_railing = Deck.create(name: 'deck-railing')

patio_cover = Deck.create(name: 'patio-cover')

fences_and_railing = Deck.create(name: 'fences-and-railing')

ipe_decks = Deck.create(name: 'ipe-decks')
i = 1
Dir.foreach("#{Rails.root}/app/assets/images/decks-ipe") do |item|
  next if item == '.' or item == '..'
  ipe_decks.images.create({pic: File.new("#{Rails.root}/app/assets/images/decks-ipe/#{item}"), position: i})
  i += 1
end

azek_decks = Deck.create(name: 'azek-decks')
i = 1
Dir.foreach("#{Rails.root}/app/assets/images/decks-azek") do |item|
  next if item == '.' or item == '..'
  azek_decks.images.create({pic: File.new("#{Rails.root}/app/assets/images/decks-azek/#{item}"), position: i})
  i += 1
end

trex_decks = Deck.create(name: 'trex-decks')
i = 1
Dir.foreach("#{Rails.root}/app/assets/images/decks-trex") do |item|
  next if item == '.' or item == '..'
  trex_decks.images.create({pic: File.new("#{Rails.root}/app/assets/images/decks-trex/#{item}"), position: i})
  i += 1
end

mangaris_decks = Deck.create(name: 'mangaris-decks')
i = 1
Dir.foreach("#{Rails.root}/app/assets/images/decks-mangaris") do |item|
  next if item == '.' or item == '..'
  mangaris_decks.images.create({pic: File.new("#{Rails.root}/app/assets/images/decks-mangaris/#{item}"), position: i})
  i += 1
end

other_projects = Deck.create(name: 'other-projects')



















